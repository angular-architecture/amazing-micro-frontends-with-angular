import { ErrorHandler, Injectable } from '@angular/core';
import { LoggingService } from '@ijs-london/logging';
import { ErrorHandlingOptions } from './error-handling-options';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlingService implements ErrorHandler {

  constructor(
    private options: ErrorHandlingOptions,
    private logger: LoggingService
  ) { }

  handleError(error: any): void {
    this.logger.error(`${this.options.applicationName}: ${error}`);
  }
}

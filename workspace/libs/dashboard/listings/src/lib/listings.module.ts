import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoggingService } from '@ijs-london/logging';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        loadChildren: () =>
          import('./manage-listings/manage-listings.module').then(
            (m) => m.ManageListingsModule
          ),
      },
      /* {path: '', pathMatch: 'full', component: InsertYourComponentHere} */
    ]),
  ],
})
export class ListingsModule {
  constructor(
    private logger: LoggingService
  ) {
    this.logger.log('Loading [ListingsModule] now.');
  }
}

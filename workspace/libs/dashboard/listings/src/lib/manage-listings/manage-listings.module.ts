import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { ManageListingsRoutingModule } from './manage-listings-routing.module';
import { ManageListingsComponent } from './manage-listings.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';


const routes: Routes = [
  { path: '', component: ManageListingsComponent }
];

@NgModule({
  declarations: [
    ManageListingsComponent
  ],
  imports: [
    CommonModule,
    ManageListingsRoutingModule,
    RouterModule.forChild(routes),
    MatTooltipModule,
    MatCardModule
  ]
})
export class ManageListingsModule { }

import { Component, OnInit } from '@angular/core';
import { LoggingService } from '@ijs-london/logging';
import { Listing } from '@ijs-london/types'
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'ijs-london-manage-listings',
  templateUrl: './manage-listings.component.html',
  styleUrls: ['./manage-listings.component.scss']
})
export class ManageListingsComponent implements OnInit {

  private listingsSubject: BehaviorSubject<Listing[]> = new BehaviorSubject<Listing[]>(new Array<Listing>());
  public readonly listings$: Observable<Listing[]> = this.listingsSubject.asObservable();

  constructor(private logger: LoggingService) {
    this.logger.log(`Preparing to load [ManageListingsComponent] component.`);
  }

  ngOnInit(): void {
    this.logger.log(`Preparing to initialize [ManageListingsComponent] component.`);

    const listings: Listing[] = [
      {
        listingID: '1',
        image: './assets/img/card-2.jpg',
        title: 'Cozy 5 Stars Apartment',
        description: 'The place is close to Barceloneta Beach and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Barcelona.',
        priceInfo: '$899/night',
        location: 'Barcelona, Spain',
      },
      {
        listingID: '2',
        image: './assets/img/card-3.jpg',
        title: 'Office Studio',
        description: 'The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the night life in London, UK.',
        priceInfo: '$1.119/night',
        location: 'London, UK',
      },
      {
        listingID: '3',
        image: './assets/img/card-1.jpg',
        title: 'Beautiful Castle',
        description: 'The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Milan.',
        priceInfo: '$459/night',
        location: 'Milan, Italy'
      },
      {
        listingID: '1',
        image: './assets/img/card-2.jpg',
        title: 'Cozy 5 Stars Apartment',
        description: 'The place is close to Barceloneta Beach and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Barcelona.',
        priceInfo: '$899/night',
        location: 'Barcelona, Spain',
      },
      {
        listingID: '2',
        image: './assets/img/card-3.jpg',
        title: 'Office Studio',
        description: 'The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the night life in London, UK.',
        priceInfo: '$1.119/night',
        location: 'London, UK',
      },
      {
        listingID: '3',
        image: './assets/img/card-1.jpg',
        title: 'Beautiful Castle',
        description: 'The place is close to Metro Station and bus stop just 2 min by walk and near to "Naviglio" where you can enjoy the main night life in Milan.',
        priceInfo: '$459/night',
        location: 'Milan, Italy'
      }
    ];
    this.listingsSubject.next(listings);
  }

}

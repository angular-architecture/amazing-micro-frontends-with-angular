export class Listing {
    listingID!: string;
    image!: string;
    title!: string;
    description!: string;
    priceInfo!: string;
    location!: string;
}
import { Injectable } from '@angular/core';
import { datadogLogs } from '@datadog/browser-logs';
import { DataDogOptions } from './datadog-options';
import { datadogRum } from '@datadog/browser-rum';

@Injectable({
  providedIn: 'root'
})
export class LoggingService {
  start: string = new Date().toUTCString();

  constructor(private loggerOptions: DataDogOptions) {
    this.initializeLogger();
    this.initializeRealtimeUserMonitoring();
  }

  initializeRealtimeUserMonitoring() {

    datadogRum.init({
      applicationId: '183c6b58-6fa2-4a66-9381-e0007d8bd736',
      clientToken: 'pubf9712defce677a596ae5688002368f21',
      site: 'datadoghq.com',
      service: 'ijs-london-2022-dashboard',
      // Specify a version number to identify the deployed version of your application in Datadog 
      version: '1.0.0',
      sampleRate: 100,
      trackInteractions: true,
      defaultPrivacyLevel: 'mask-user-input'
    });

    datadogRum.startSessionReplayRecording();
  }

  private initializeLogger() {
    datadogLogs.init({
      clientToken: this.loggerOptions.clientToken,
      site: 'datadoghq.com',
      forwardErrorsToLogs: true,
      sampleRate: 100
    });

    datadogLogs.logger.log(`Initialized DataDog logger.`, { application: this.loggerOptions.applicationName }, 'info');
  }

  log(message: string) {
    datadogLogs.logger.info(message);
  }

  error(message: string) {
    datadogLogs.logger.error(message);
  }
}

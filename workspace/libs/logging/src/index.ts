export { LoggingModule } from './lib/logging.module';

export { LoggingService } from './lib/logging.service';
export { DataDogOptions } from './lib/datadog-options';
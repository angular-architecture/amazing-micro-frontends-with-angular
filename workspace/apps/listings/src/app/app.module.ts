// import { RemoteEntryModule } from './remote-entry/entry.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { ErrorHandlingModule } from '@ijs-london/error-handling';
import { DataDogOptions, LoggingModule } from '@ijs-london/logging';

const appRoutes: Routes = [
  {
    path: '',
    loadChildren: () => import('@ijs-london/dashboard/listings').then((m) => m.ListingsModule)
  },
  {
    path: 'entry-module',
    loadChildren: () => import('./remote-entry/entry.module').then((m) => m.RemoteEntryModule)
  }
];

/**
 * Create an account at https://www.DataDogHQ.com to get a client token.
 */
const loggingOptions: DataDogOptions = {
  applicationName: 'ijs-london-2022-listings',
  clientToken: 'pub905382b0fa24bc2f51efa742260ef110'
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    ErrorHandlingModule.forRoot({ applicationName: 'ijs-london-2022-listings' }),
    LoggingModule.forRoot(loggingOptions),
    RouterModule.forRoot(appRoutes, { initialNavigation: 'enabledBlocking' }),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

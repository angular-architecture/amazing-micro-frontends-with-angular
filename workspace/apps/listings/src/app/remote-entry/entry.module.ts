import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

import { RemoteEntryComponent } from './entry.component';

@NgModule({
  declarations: [RemoteEntryComponent],
  imports: [
    BrowserModule,
    RouterModule.forChild([
      {
        path: '',
        // component: RemoteEntryComponent,
        loadChildren: () => import('@ijs-london/dashboard/listings').then((m) => m.ListingsModule)
      },
    ]),
  ],
  providers: [],
})
export class RemoteEntryModule {}

import { Component } from '@angular/core';

@Component({
  selector: 'ijs-london-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'secure :: MFE REMOTE';
}

import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: '',
        component: AdminLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: () =>
                    import('./dashboard/dashboard.module').then((m) => m.DashboardModule),
            },
            {
                path: 'listings',
                loadChildren: () => import('@ijs-london/dashboard/listings').then((m) => m.ListingsModule),
            },
            {
                path: 'ijs-bnb',
                loadChildren: () => import('listings/Module').then((m) => m.RemoteEntryModule)
            },
            {
                path: 'components',
                loadChildren: () =>
                    import('./components/components.module').then(
                        (m) => m.ComponentsModule
                    ),
            },
            {
                path: 'maps',
                loadChildren: () =>
                    import('./maps/maps.module').then((m) => m.MapsModule),
            },
            {
                path: '',
                loadChildren: () =>
                    import('./userpage/user.module').then((m) => m.UserModule),
            },
            {
                path: '',
                loadChildren: () =>
                    import('./timeline/timeline.module').then((m) => m.TimelineModule),
            },
        ],
    },
    {
        path: '',
        component: AuthLayoutComponent,
        children: [
            {
                path: 'pages',
                loadChildren: () =>
                    import('./pages/pages.module').then((m) => m.PagesModule),
            },
        ],
    },
];

import { Component } from '@angular/core';

@Component({
  selector: 'ijs-london-root',
  templateUrl: './app.component.html',
  //   template: `
  //   <header class="flex">
  //   <h1>Welcome to {{ title }}!</h1>
  // </header>
  // <router-outlet></router-outlet>
  // `,
  styleUrls: ['./app.component.scss'],
  // styles: [``]
})
export class AppComponent {
  title = 'admin :: MFE HOST';
}

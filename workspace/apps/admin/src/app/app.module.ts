import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      [
        {
          path: '', // FIXME: ADD ROUTE [EMPTY TO LOAD BY DEFAULT] OR WITH [PATH] TO LOAD ON NAVIGATE
          loadChildren: () =>
            import('secure/Module').then((m) => m.RemoteEntryModule), // a virtual path-map to a remote
        },
        // {
        //   path: 'secure', // FIXME: ADD ROUTE [EMPTY TO LOAD BY DEFAULT] OR WITH [PATH] TO LOAD ON NAVIGATE
        //   loadChildren: () =>
        //     import('secure/Module').then((m) => m.RemoteEntryModule),
        // },
      ],
      { initialNavigation: 'enabledBlocking' }
    ),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}

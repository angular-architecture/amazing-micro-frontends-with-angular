const ModuleFederationPlugin = require('webpack/lib/container/ModuleFederationPlugin');
const mf = require('@angular-architects/module-federation/webpack');
const path = require('path');

/**
 * We use the NX_TSCONFIG_PATH environment variable when using the @nrwl/angular:webpack-browser
 * builder as it will generate a temporary tsconfig file which contains any required remappings of
 * shared libraries.
 * A remapping will occur when a library is buildable, as webpack needs to know the location of the
 * built files for the buildable library.
 * This NX_TSCONFIG_PATH environment variable is set by the @nrwl/angular:webpack-browser and it contains
 * the location of the generated temporary tsconfig file.
 */
const tsConfigPath =
  process.env.NX_TSCONFIG_PATH ??
  path.join(__dirname, '../../tsconfig.base.json');

const workspaceRootPath = path.join(__dirname, '../../');
const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  tsConfigPath,
  [
    /* mapped paths to share */
  ],
  workspaceRootPath
);

module.exports = {
  output: {
    uniqueName: 'admin', // represents the host or remote in the generated bundles. By default, webpack uses the name from package.json for this. In order to avoid name conflicts when using Monorepos with several applications, it is recommended to set the uniqueName manually.
    publicPath: 'auto', // defines the URL under which the application can later be found. This reveals where the individual bundles of the application but also their assets, e.g. pictures or styles, can be found.
  },
  optimization: {
    runtimeChunk: false,
    minimize: false,
  },
  resolve: {
    alias: {
      ...sharedMappings.getAliases(),
    },
  },
  plugins: [
    // USE TO AVOID CREATING JS BUNDLES; DEFINE TARGET REMOTE BUNDLES (E.G., MODULES)
    new ModuleFederationPlugin({
      remotes: {
        secure: 'secure@http://localhost:4201/remoteEntry.js',
      },
      // webpack emit a runtime error when the shell and the micro frontend(s) need different incompetible versions (e. g. two different major versions).
      shared: {
        // FIXME: 1. DEFINE WHAT THE [HOST] WILL SHARE WITH [REMOTES] HERE
        // FIXME: 2. HOST AND REMOTE AGREE ON COMMON VERSION OF SHARED
        '@angular/core': { singleton: true, strictVersion: true },
        '@angular/common': { singleton: true, strictVersion: true },
        '@angular/common/http': { singleton: true, strictVersion: true },
        '@angular/router': { singleton: true, strictVersion: true },
        ...sharedMappings.getDescriptors(),
      },
    }),
    sharedMappings.getPlugin(),
  ],
};
